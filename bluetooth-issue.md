# Bluetooth Issue

1. Edit the `/etc/pulseaudio/default.pa`.

```sh
sudo vim /etc/pulseaudio/default.pa
```
2. Add the line.

```sh
load-module module-switch-on-connect
```

3. Restart Pulse Audio

```sh
systemctl --user restart pulseaudio
```
