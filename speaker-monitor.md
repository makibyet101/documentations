# Activate Your Monitor's Speaker

1. Determine Which Card you're running.
```sh
pactl list cards
```
2. Determine which sink to use.

```sh
pactl list sinks
```
3. Get the sink and card.
```sh
pactl set-card-profile <sink> <card>
```
Example

```sh
pactl set-card-profile alsa_card.pci-0000_00_1b.0 output:hdmi-stereo+input:analog-stereo
```
