# Backup

1. List Keys and Determine which Keys to Backup

```sh
gpg --list-secret-keys --keyid-format LONG
```
```
/home/user/.local/share/gnupg/pubring.kbx
-----------------------------------------
sec   rsa4096 2023-03-07 [SC]
      FFBF66E7EECCF7B5639C83DE57C6F11DD1B8F4A4
uid           [ultimate] Mark Nhel A. Besonia (Main Account) <makibyet101@gmail.com>
ssb   rsa4096 2023-03-07 [E]
```

2. Export the private GPG key.

```sh
gpg -o private.gpg --export-options backup --export-secret-keys makibyet101@gmail.com
```
3. Enter the passphrase of your GPG key.


# Restore

1. Import the private key

```sh
gpg --import-options restore --import private.gpg
```
2. Enter the passphrase of the private key.

3. Edit the imported Key.

```sh
gpg --edit-key makibyet101@gmail.com
```

4. Trust the key.

```sh
gpg> trust
```
