# Add GPG Keys to Sign Commits

1. Create A GPG Key

```sh
gpg --gen-key

gpg --full-gen-key
```

2. GPG Key ID

```sh
gpg --list-secret-keys --keyid-format LONG json@domain.org
```
```sh
sec   rsa4096/57C6141KE08F4A4 1999-03-07 [SC]
      FFPA66E7PPCCF7B5639C83DE57C6F31MA1OAF4A4
uid                 [ultimate] John Son <json@domain.org>
ssb   rsa4096/2M91C3D3AKAE3F69 1999-03-07 [E]
```
The GPG Key ID is the characters after `sec rsa4096/` in this case `57C6141KE08F4A4`

3. Add your GPG Key into your account

You can navigate in your `Profile` > `GPG Keys` and add the GPG Public Key which you can generate through

```sh
gpg --armor --export <ID>
```

4. Add your GPG Key to your configuration
```sh
git config --global user.signingkey <ID>
```
5. Tell git that you want to sign your commit.

Manually
```sh
git commit -S -m "My commit message"
```
Automatically

```sh
git config --global commit.gpgsign true
```
*Note:*
- This option will sign all your commits in all your local repositories.
- If you have a couple of GPG Keys, you can also set signingkey to some repositories

```sh
git config --local user.signingkey <ID>
```
